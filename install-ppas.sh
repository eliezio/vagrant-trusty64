#!/usr/bin/env bash

set -e -x

REQUIRED_PPAS="ppa:git-core/ppa ppa:ebo/cvs2git ppa:george-edison55/cmake-3.x"

declare -A ppa_list
for ppa in $REQUIRED_PPAS; do ppa_list[$ppa]=1; done

for ppa in $(for apt in $(find /etc/apt/ -name \*.list); do
    grep -o "^deb http://ppa.launchpad.net/[a-z0-9\-]\+/[a-z0-9\-]\+" $apt | while read entry; do
        ld_id=$(echo $entry | cut -d/ -f4)
        ppa_id=$(echo $entry | cut -d/ -f5)
        echo ppa:$ld_id/$ppa_id
    done
done); do unset ppa_list[$ppa]; done

for ppa in "${!ppa_list[@]}"; do
    sudo add-apt-repository -y $ppa
done
